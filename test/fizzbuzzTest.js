const assert = require('chai').assert;
const fizzbuzz = require('../fizzbuzz');

const case1 = fizzbuzz(1);
const case2 = fizzbuzz(3);
const case3 = fizzbuzz(5);
const case4 = fizzbuzz(15);
const case5 = fizzbuzz(45);
const case6 = fizzbuzz('12');

describe('nth fizzbuzz',() => {
    it('equals 1',() => {
        assert.equal(case1,1);
    });
    it('equals Fizz',() => {
        assert.equal(case2,'Fizz');
    });
    it('equals Buzz',() => {
        assert.equal(case3,'Buzz');
    });
    it('equals FizzBuzz',() => {
        assert.equal(case4,'FizzBuzz');
    });
    it('equals FizzBuzz',() => {
        assert.equal(case5,'FizzBuzz');
    });
    it('equals Invalid Number',() => {
        assert.equal(case6,'Invalid Number');
    });
})


