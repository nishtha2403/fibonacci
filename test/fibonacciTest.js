const assert = require('chai').assert;
const fibonacci = require('../fibonacci');

const case1 = fibonacci(0);
const case2 = fibonacci(1);
const case3 = fibonacci('3');
const case4 = fibonacci(5);
const case5 = fibonacci(10);
const case6 = fibonacci(16);


describe('nth fibonacci number',() => {
    it('equals 0', () => {
        assert.equal(case1,0);
    });
    it('equals 1', () => {
        assert.equal(case2,1);
    });
    it('invalid', () => {
        assert.equal(case3,'Invalid Number');
    });
    it('equals 5', () => {
        assert.equal(case4,5);
    });
    it('equals 55', () => {
        assert.equal(case5,55);
    });
    it('equals 987', () => {
        assert.equal(case6,987);
    });
});
