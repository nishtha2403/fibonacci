let fibonacci = (n) => {
    if(Number.isInteger(n)){
        let a=0,b=1,c;
        if(n === 0) {
            return a;
        }
        if(n === 1) {
            return a+b;
        } 
        for(let i=1;i<n;i++){
            c=a+b;
            a=b;
            b=c;
        }
        return c;
    }
    return 'Invalid Number';
}

module.exports = fibonacci;